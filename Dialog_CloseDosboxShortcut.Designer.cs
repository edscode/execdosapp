﻿namespace ProGammaX
{
    partial class Dialog_CloseDosboxShortcut
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.TableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.OK_Button = new myButton();
            this.Close_Button = new myButton();
            this.checkBox_DoNotShowThisMessageAgain = new myCheckBox();
            this.TableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(538, 73);
            this.label1.TabIndex = 0;
            this.label1.Text = "You can switch between fullscreen and window mode by pressing \"Alt + Enter\" and y" +
    "ou can close DOSBox at any time by pressing \"Ctrl + F9\". \r\n \r\nYou can find all D" +
    "OSBox special keys in the Help menu...";
            // 
            // TableLayoutPanel1
            // 
            this.TableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.TableLayoutPanel1.ColumnCount = 2;
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TableLayoutPanel1.Controls.Add(this.OK_Button, 0, 0);
            this.TableLayoutPanel1.Controls.Add(this.Close_Button, 1, 0);
            this.TableLayoutPanel1.Location = new System.Drawing.Point(354, 86);
            this.TableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.TableLayoutPanel1.Name = "TableLayoutPanel1";
            this.TableLayoutPanel1.RowCount = 1;
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.TableLayoutPanel1.Size = new System.Drawing.Size(195, 36);
            this.TableLayoutPanel1.TabIndex = 4;
            // 
            // OK_Button
            // 
            this.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.OK_Button.Location = new System.Drawing.Point(4, 4);
            this.OK_Button.Margin = new System.Windows.Forms.Padding(4);
            this.OK_Button.Name = "OK_Button";
            this.OK_Button.Size = new System.Drawing.Size(89, 28);
            this.OK_Button.TabIndex = 0;
            this.OK_Button.Text = "OK";
            this.OK_Button.Visible = false;
            // 
            // Close_Button
            // 
            this.Close_Button.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Close_Button.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close_Button.Location = new System.Drawing.Point(101, 4);
            this.Close_Button.Margin = new System.Windows.Forms.Padding(4);
            this.Close_Button.Name = "Close_Button";
            this.Close_Button.Size = new System.Drawing.Size(89, 28);
            this.Close_Button.TabIndex = 1;
            this.Close_Button.Text = "Close";
            this.Close_Button.Click += new System.EventHandler(this.Close_Button_Click);
            // 
            // checkBox_DoNotShowThisMessageAgain
            // 
            this.checkBox_DoNotShowThisMessageAgain.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.checkBox_DoNotShowThisMessageAgain.AutoSize = true;
            this.checkBox_DoNotShowThisMessageAgain.Location = new System.Drawing.Point(12, 97);
            this.checkBox_DoNotShowThisMessageAgain.Name = "checkBox_DoNotShowThisMessageAgain";
            this.checkBox_DoNotShowThisMessageAgain.Size = new System.Drawing.Size(238, 21);
            this.checkBox_DoNotShowThisMessageAgain.TabIndex = 5;
            this.checkBox_DoNotShowThisMessageAgain.Text = "Do not show this message again.";
            this.checkBox_DoNotShowThisMessageAgain.UseVisualStyleBackColor = true;
            // 
            // Dialog_CloseDosboxShortcut
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(562, 135);
            this.Controls.Add(this.checkBox_DoNotShowThisMessageAgain);
            this.Controls.Add(this.TableLayoutPanel1);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Dialog_CloseDosboxShortcut";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Did You know...";
            this.TableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Label label1;
        internal System.Windows.Forms.TableLayoutPanel TableLayoutPanel1;
        internal myButton OK_Button;
        internal myButton Close_Button;
        public myCheckBox checkBox_DoNotShowThisMessageAgain;
    }
}
