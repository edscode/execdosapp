#region Header

// VBConversions Note: VB project level imports

#endregion Header


namespace ProGammaX
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics;
    using System.Drawing;
    using System.Windows.Forms;
    using System.Xml.Linq;

  

    public partial class Dialog_LoadTemplate
    {

        private static Dialog_LoadTemplate Dialog_LoadTemplateRef;
        public static Dialog_LoadTemplate dialog_LoadTemplateRef
            {
            get
                {
                return Dialog_LoadTemplateRef;
                }
            }


        #region Constructors

        public Dialog_LoadTemplate() : base(true, false, true)
        {
            InitializeComponent();

            ProGammaX.MainForm.mainFormRef.applySelectedTheme(this);

            Dialog_LoadTemplateRef = this;



            this.Cancel_Button.Click += Cancel_Button_Click;
            this.OK_Button.Click += OK_Button_Click;
            this.ComboBox1.SelectedIndexChanged += ComboBox1_SelectedIndexChanged;


        }

        #endregion Constructors

        #region Methods

        public void Cancel_Button_Click(System.Object sender, System.EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        public void ComboBox1_SelectedIndexChanged(System.Object sender, System.EventArgs e)
        {
            ProGammaX.MainForm.mainFormRef.ShowTemplateDialog();
        }

  

        public void OK_Button_Click(System.Object sender, System.EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        #endregion Methods
    }
}