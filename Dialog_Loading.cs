﻿namespace ProGammaX
{
    using System;
    using System.Runtime.InteropServices;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    public partial class Dialog_Loading
    {

        private static Dialog_Loading Dialog_LoadingRef;
        public static Dialog_Loading dialog_LoadingRef
        {
            get
            {
                return Dialog_LoadingRef;
            }
        }

        ////removes X button
        //private const int CP_NOCLOSE_BUTTON = 0x200;
        //protected override CreateParams CreateParams
        //{
        //    get
        //    {
        //        CreateParams myCp = base.CreateParams;
        //        myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
        //        return myCp;
        //    }
        //}
        ////removes all top menu
        //private const int WS_SYSMENU = 0x80000;
        //protected override CreateParams CreateParams
        //{
        //    get
        //    {
        //        CreateParams cp = base.CreateParams;
        //        cp.Style &= ~WS_SYSMENU;
        //        return cp;
        //    }
        //}




        //public bool prolongedOperationIsRunning = false;









        #region Constructors

        public Dialog_Loading() : base(false, false, false)
        {
            InitializeComponent();

            StartPosition = FormStartPosition.Manual;
            this.Location = new System.Drawing.Point((MainForm.mainFormRef.Location.X + MainForm.mainFormRef.Size.Width / 2 - this.Size.Width / 2), (MainForm.mainFormRef.Location.Y + MainForm.mainFormRef.Size.Height / 2) - this.Size.Height / 2); // center parent (MainForm)

            this.Cancel_Button.KeyDown += Cancel_Button_Click;

            ProGammaX.MainForm.mainFormRef.applySelectedTheme(this);

            Dialog_LoadingRef = this;
        }

        #endregion Constructors

        #region Methods

        protected override void OnShown(EventArgs e)
        {

            MainForm.mainFormRef.prolongedOperationIsRunning = true;

            base.OnShown(e);
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            MainForm.mainFormRef.prolongedOperationIsRunning = false;
        }

        // ERROR: Handles clauses are not supported in C#
        private void Cancel_Button_Click(System.Object sender, System.EventArgs e)
        {


            if (MainForm.mainFormRef.prolongedOperationIsRunning == true)
            {

                DialogResult dialogResult = new DialogResult();
                dialogResult = new ShowMessage().Show(MainForm.StringToBeTranslated[186], "ProGammaX", enumMessageButton.YesNo);


                if (dialogResult == System.Windows.Forms.DialogResult.Yes)
                {
                    //MainForm.mainFormRef.backgroundWorker.WorkerSupportsCancellation = true;
                    MainForm.mainFormRef.backgroundWorker2.CancelAsync();
                    MainForm.mainFormRef.backgroundWorker2IsCanceling = true;
                    MainForm.mainFormRef.MaximizeBox = true;
                    MainForm.mainFormRef.MinimizeBox = true;
                    this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
                    this.Close();
                    MainForm.mainFormRef.prolongedOperationIsRunning = false;
                }
                else
                {
                    this.Show();
                    this.Activate();
                    return;
                }

            }
            else
            {
                this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
                this.Close();
                MainForm.mainFormRef.prolongedOperationIsRunning = false;
            }

        }



        // ERROR: Handles clauses are not supported in C#
        private void OK_Button_Click(System.Object sender, System.EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        #endregion Methods
    }
}