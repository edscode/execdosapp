﻿namespace ProGammaX
{
    using System;
    using System.Runtime.InteropServices;
    using System.Windows.Forms;

    public partial class Dialog_MultiprofileSavingProgress
    {

        private static Dialog_MultiprofileSavingProgress Dialog_MultiprofileSavingProgressRef;
        public static Dialog_MultiprofileSavingProgress dialog_MultiprofileSavingProgressRef
        {
        get
            {
            return Dialog_MultiprofileSavingProgressRef;
            }
        }

        ////removes X button
        //private const int CP_NOCLOSE_BUTTON = 0x200;
        //protected override CreateParams CreateParams
        //{
        //    get
        //    {
        //        CreateParams myCp = base.CreateParams;
        //        myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
        //        return myCp;
        //    }
        //}
        ////removes all top menu
        //private const int WS_SYSMENU = 0x80000;
        //protected override CreateParams CreateParams
        //{
        //    get
        //    {
        //        CreateParams cp = base.CreateParams;
        //        cp.Style &= ~WS_SYSMENU;
        //        return cp;
        //    }
        //}




        //public bool prolongedOperationIsRunning = false;









        #region Constructors

        public Dialog_MultiprofileSavingProgress() : base(false, false, false)
        {

            //this.ControlBox = false;
            InitializeComponent();

            StartPosition = FormStartPosition.Manual;
            this.Location = new System.Drawing.Point((MainForm.mainFormRef.Location.X + MainForm.mainFormRef.Size.Width / 2 - this.Size.Width / 2), (MainForm.mainFormRef.Location.Y + MainForm.mainFormRef.Size.Height / 2) - this.Size.Height / 2); // center parent (MainForm)

            this.Cancel_Button.KeyDown += Cancel_Button_Click;  
 

            ProGammaX.MainForm.mainFormRef.applySelectedTheme(this);

            Dialog_MultiprofileSavingProgressRef = this;
        }

        #endregion Constructors

        #region Methods

        //protected override void OnVisibleChanged(EventArgs e)
        //{
        //    base.OnVisibleChanged(e);


            

        //}

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);

            MainForm.mainFormRef.prolongedOperationIsRunning = true;

        }



        // ERROR: Handles clauses are not supported in C#
        private void Cancel_Button_Click(System.Object sender, System.EventArgs e)
        {


            if (MainForm.mainFormRef.prolongedOperationIsRunning == true)
            {

                DialogResult dialogResult = new DialogResult();
                dialogResult = new ShowMessage().Show(MainForm.StringToBeTranslated[186], "ProGammaX", enumMessageButton.YesNo);


                if (dialogResult == System.Windows.Forms.DialogResult.Yes)
                {
                    //MainForm.mainFormRef.backgroundWorker.WorkerSupportsCancellation = true;
                    MainForm.mainFormRef.backgroundWorker.CancelAsync();
                    MainForm.mainFormRef.backgroundWorkerIsCanceling = true;
                    MainForm.mainFormRef.MaximizeBox = true;
                    MainForm.mainFormRef.MinimizeBox = true;
                    this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
                    this.Close();
                    MainForm.mainFormRef.prolongedOperationIsRunning = false;
                }
                else
                {
                    this.Show();
                    this.Activate();
                    return;
                }

            }
            else
            {
                this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
                this.Close();
                MainForm.mainFormRef.prolongedOperationIsRunning = false;
            }

        }

        // ERROR: Handles clauses are not supported in C#
        private void Dialog_MultiprofileSavingProgress_Load(System.Object sender, System.EventArgs e)
        {
            //Me.TopMost = True
            //Me.TopLevel = True

            
            //this.Location = this.Owner.Location;
            // vycentruje okno do stredu obrazovky
            // Me.SetBounds((System.Windows.Forms.Screen.GetBounds(Me).Width / 2) - (Me.Width / 2), _
            //(System.Windows.Forms.Screen.GetBounds(Me).Height / 2) - (Me.Height / 2), _
            //Me.Width, Me.Height, System.Windows.Forms.BoundsSpecified.Location)
        }

        // ERROR: Handles clauses are not supported in C#
        private void OK_Button_Click(System.Object sender, System.EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        #endregion Methods
    }
}