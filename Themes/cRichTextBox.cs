﻿    namespace ProGammaX
    {
        #region Directives
        using System;
        using System.Windows.Forms;
        using System.Runtime.InteropServices;
        using System.Drawing;
        #endregion

        [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust")]
        public class cRichTextBox : IDisposable
        {
            #region Fields
            private IntPtr _hRichTextboxWnd = IntPtr.Zero;
            private cInternalScrollBar _cInternalScroll;
            #endregion

            #region Constructor
            public cRichTextBox(IntPtr handle, Bitmap hztrack, Bitmap hzarrow, Bitmap hzthumb, Bitmap vttrack, Bitmap vtarrow, Bitmap vtthumb, Bitmap fader)
            {
                if (handle == IntPtr.Zero)
                    throw new Exception("The RichTextBox handle is invalid.");
                _hRichTextboxWnd = handle;
                if (hztrack != null && hzarrow != null && hzthumb != null && vttrack != null && vtarrow != null && vtthumb != null)
                    _cInternalScroll = new cInternalScrollBar(_hRichTextboxWnd, hztrack, hzarrow, hzthumb, vttrack, vtarrow, vtthumb, fader);
                else
                    throw new Exception("The RichTextBox image(s) are invalid");
            }

            public void Dispose()
            {
                try
                {
                    if (_cInternalScroll != null) _cInternalScroll.Dispose();
                }
                catch { }
            }
            #endregion






    }


}
