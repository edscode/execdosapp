﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace ProGammaX
{
    [TypeConverter(typeof(CustomBaseUIAppearanceTypeConverter))]
    public class CustomButtonUIAppearance : CustomBaseUIAppearance
    {
        private Color _mouseHoverBorderColor;
        private Color _mouseHoverBackColor;
        private Color _mouseHoverForeColor;
        private Color _mouseDownBorderColor;
        private Color _mouseDownBackColor;
        private Color _mouseDownForeColor;
        private Color _checkedBorderColor;
        private Color _checkedBackColor;
        private Color _checkedForeColor;
        private int _borderAngle;

        [Description("Gets or sets button’s border color when mouse hovers the button."), Category("Custom Appearance"), DefaultValue(typeof(Color), "Black")]
        public Color MouseHoverBorderColor
        {
            get { return _mouseHoverBorderColor; }
            set
            {
                if (value.Equals(Color.Transparent))
                {
                    throw new ArgumentOutOfRangeException("MouseHoverBorderColor", "Parameter cannot be set to Color.Transparent");
                }
                if (!_mouseHoverBorderColor.Equals(value))
                {
                    _mouseHoverBorderColor = value;
                    Owner.Invalidate();
                }
            }
        }

        [Description("Gets or sets button’s background color when mouse hovers the button."), Category("Custom Appearance"), DefaultValue(typeof(Color), "White")]
        public Color MouseHoverBackColor
        {
            get { return _mouseHoverBackColor; }
            set
            {
                if (value.Equals(Color.Transparent))
                {
                    throw new ArgumentOutOfRangeException("MouseHoverBackColor", "Parameter cannot be set to Color.Transparent");
                }
                if (!_mouseHoverBackColor.Equals(value))
                {
                    _mouseHoverBackColor = value;
                    Owner.Invalidate();
                }
            }
        }

        [Description("Gets or sets button’s fore color when mouse hovers the button."), Category("Custom Appearance"), DefaultValue(typeof(Color), "Black")]
        public Color MouseHoverForeColor
        {
            get { return _mouseHoverForeColor; }
            set
            {
                if (value.Equals(Color.Transparent))
                {
                    throw new ArgumentOutOfRangeException("MouseHoverForeColor", "Parameter cannot be set to Color.Transparent");
                }
                if (!_mouseHoverForeColor.Equals(value))
                {
                    _mouseHoverForeColor = value;
                    Owner.Invalidate();
                }
            }
        }

        [Description("Gets or sets button’s border color when mouse down."), Category("Custom Appearance"), DefaultValue(typeof(Color), "Black")]
        public Color MouseDownBorderColor
        {
            get { return _mouseDownBorderColor; }
            set
            {
                if (value.Equals(Color.Transparent))
                {
                    throw new ArgumentOutOfRangeException("MouseDownBorderColor", "Parameter cannot be set to Color.Transparent");
                }
                if (!_mouseDownBorderColor.Equals(value))
                {
                    _mouseDownBorderColor = value;
                    Owner.Invalidate();
                }
            }
        }

        [Description("Gets or sets button’s background color when mouse down."), Category("Custom Appearance"), DefaultValue(typeof(Color), "White")]
        public Color MouseDownBackColor
        {
            get { return _mouseDownBackColor; }
            set
            {
                if (value.Equals(Color.Transparent))
                {
                    throw new ArgumentOutOfRangeException("MouseDownBackColor", "Parameter cannot be set to Color.Transparent");
                }
                if (!_mouseDownBackColor.Equals(value))
                {
                    _mouseDownBackColor = value;
                    Owner.Invalidate();
                }
            }
        }

        [Description("Gets or sets button’s fore color when mouse down."), Category("Custom Appearance"), DefaultValue(typeof(Color), "Black")]
        public Color MouseDownForeColor
        {
            get { return _mouseDownForeColor; }
            set
            {
                if (value.Equals(Color.Transparent))
                {
                    throw new ArgumentOutOfRangeException("MouseDownForeColor", "Parameter cannot be set to Color.Transparent");
                }
                if (!_mouseDownForeColor.Equals(value))
                {
                    _mouseDownForeColor = value;
                    Owner.Invalidate();
                }
            }
        }

        [Description("Gets or sets button’s border color when its state is checked."), Category("Custom Appearance"), DefaultValue(typeof(Color), "Black")]
        public Color CheckedBorderColor
        {
            get { return _checkedBorderColor; }
            set
            {
                if (value.Equals(Color.Transparent))
                {
                    throw new ArgumentOutOfRangeException("CheckedBorderColor", "Parameter cannot be set to Color.Transparent");
                }
                if (!_checkedBorderColor.Equals(value))
                {
                    _checkedBorderColor = value;
                    Owner.Invalidate();
                }
            }
        }

        [Description("Gets or sets button’s background color when its state is checked."), Category("Custom Appearance"), DefaultValue(typeof(Color), "White")]
        public Color CheckedBackColor
        {
            get { return _checkedBackColor; }
            set
            {
                if (value.Equals(Color.Transparent))
                {
                    throw new ArgumentOutOfRangeException("CheckedBackColor", "Parameter cannot be set to Color.Transparent");
                }
                if (!_checkedBackColor.Equals(value))
                {
                    _checkedBackColor = value;
                    Owner.Invalidate();
                }
            }
        }

        [Description("Gets or sets button’s fore color when its state is checked."), Category("Custom Appearance"), DefaultValue(typeof(Color), "Black")]
        public Color CheckedForeColor
        {
            get { return _checkedForeColor; }
            set
            {
                if (value.Equals(Color.Transparent))
                {
                    throw new ArgumentOutOfRangeException("CheckedForeColor", "Parameter cannot be set to Color.Transparent");
                }
                if (!_checkedForeColor.Equals(value))
                {
                    _checkedForeColor = value;
                    Owner.Invalidate();
                }
            }
        }

        [Description("Gets or Sets button border’s angle."), Category("Custom Appearance"), DefaultValue(3)]
        public int BorderAngle
        {
            get { return _borderAngle; }
            set
            {
                if (value < 0) { throw new ArgumentOutOfRangeException("BorderAngle", "Parameter must be set to a valid integer value hihger than or equal to 0."); }
                if (_borderAngle != value)
                {
                    _borderAngle = value; Owner.Invalidate();
                }
            }
        }
        public CustomButtonUIAppearance(Control owner) : base(owner)
        {
            _mouseHoverBorderColor = Color.Black;
            _mouseHoverBackColor = Color.White;
            _mouseHoverForeColor = Color.Black;
            _mouseDownBorderColor = Color.Black;
            _mouseDownBackColor = Color.White;
            _mouseDownForeColor = Color.Black;
            _checkedBorderColor = Color.Black;
            _checkedBackColor = Color.White;
            _checkedForeColor = Color.Black;
            _borderAngle = 3;
        }
    }

    public class myButton : Button
    {
        private bool _enableCustomAppearance;
        private bool _checked;
        private ButtonEvents _currentEvent;
        private EventHandler _checkedStatusChanged;

        public event EventHandler CheckedStatusChanged
        {
            add { _checkedStatusChanged += value; }
            remove { _checkedStatusChanged -= value; }
        }

        [Description("If true control's is drawn using custom UI appearance, otherwise it appears using standard drawing."), Category("Custom Appearance"),
        DefaultValue(true), DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public bool EnableCustomAppearance
        {
            get { return _enableCustomAppearance; }
            set
            {
                if (_enableCustomAppearance != value)
                {
                    _enableCustomAppearance = value;
                    if (_enableCustomAppearance)
                    {
                        AutoSize = false;
                    }
                }
            }
        }

        [Description("This property specifies the way of drawing the control."), Category("Custom Appearance"),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public CustomButtonUIAppearance CustomAppearance
        {
            get;
            set;
        }

        [Description("Get or sets the checked state."), Category("Custom Appearance"),
        DefaultValue(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public bool Checked
        {
            get { return _checked; }
            set
            {
                if (_checked != value)
                {
                    _checked = value;
                    Invalidate();
                    OnCheckedStatusChanged(new EventArgs());
                }
            }
        }

        public myButton() : base()
        {
            _enableCustomAppearance = true;
            _currentEvent = ButtonEvents.NoEvent;
            _checked = false;
        }

        protected virtual void OnCheckedStatusChanged(EventArgs args)
        {
            if (_checkedStatusChanged != null)
            {
                _checkedStatusChanged(this, args);
            }
        }

        protected override void OnMouseEnter(EventArgs e)
        {
            _currentEvent = ButtonEvents.MouseEnter;
            Invalidate();
            base.OnMouseEnter(e);
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            _currentEvent = ButtonEvents.MouseLeave;
            Invalidate();
            base.OnMouseLeave(e);
        }

        protected override void OnMouseDown(MouseEventArgs mevent)
        {
            _currentEvent = ButtonEvents.MouseDown;
            Invalidate();
            base.OnMouseDown(mevent);
        }

        protected override void OnMouseUp(MouseEventArgs mevent)
        {
            _currentEvent = ButtonEvents.MouseUp;
            Invalidate();
            base.OnMouseUp(mevent);
        }

        protected override void OnPaint(PaintEventArgs pevent)
        {
            // Call base class' OnPaint.
            base.OnPaint(pevent);

            try
            {
                // MainForm.mainFormRef.IsApplicationIdle(this);

                if (!MainForm.mainFormRef.checkBox_OPTIONS_Themes_UseSkinOnControls.Checked)
                {
                    return;

                }
            }
            catch
            {
                return;
            }


            if (!_enableCustomAppearance)
            {
                return;
            }

            int offset = 1;
            Color borderColor;
            Color backColor;
            Color foreColor;

            switch (_currentEvent)
            {
                case ButtonEvents.MouseDown:
                    borderColor = CustomAppearance.MouseDownBorderColor;
                    backColor = CustomAppearance.MouseDownBackColor;
                    foreColor = CustomAppearance.MouseDownForeColor;
                    break;
                case ButtonEvents.MouseEnter:
                case ButtonEvents.MouseUp:
                    borderColor = CustomAppearance.MouseHoverBorderColor;
                    backColor = CustomAppearance.MouseHoverBackColor;
                    foreColor = CustomAppearance.MouseHoverForeColor;
                    break;
                default: // MouseLeave and NoEvent
                    if (_checked)
                    {
                        borderColor = CustomAppearance.CheckedBorderColor;
                        backColor = CustomAppearance.CheckedBackColor;
                        foreColor = CustomAppearance.CheckedForeColor;
                    }
                    else
                    {
                        borderColor = CustomAppearance.BorderColor;
                        backColor = CustomAppearance.BackColor;
                        foreColor = ForeColor;
                    }
                    break;
            }

            Graphics graphics = pevent.Graphics;
            graphics.Clear(BackColor);
            // get Text measure according to selected Font
            SizeF stringMeasure = graphics.MeasureString(Text, Font);
            // Set graphics object to paint nice using antialias.
            if (CustomAppearance.EnableAntiAlias)
            {
                graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            }

            // ++ Calculate drawing string offsets ++
            int leftOffset = (int)(ClientRectangle.Width - stringMeasure.Width) / 2;
            int topOffset = (int)(ClientRectangle.Height - stringMeasure.Height) / 2 + offset;

            if (leftOffset < 0)
            {
                leftOffset = offset + Padding.Left;
            }
            else
            {
                leftOffset += Padding.Left;
            }
            if (topOffset < 0)
            {
                topOffset = offset + Padding.Top;
            }
            else
            {
                topOffset += Padding.Top;
            }
            // -- Calculate drawing string offsets --

            float drawingAngleSize = CustomAppearance.BorderAngle + CustomAppearance.BorderThicness;
            float drawingWidth = Width - CustomAppearance.BorderThicness;
            float drawingHeight = Height - CustomAppearance.BorderThicness;

            // Points used to draw and fill button rectangle
            PointF[] points = new PointF[9]
            {
                new PointF(drawingAngleSize, CustomAppearance.BorderThicness),
                new PointF(drawingWidth - CustomAppearance.BorderAngle - CustomAppearance.BorderThicness, CustomAppearance.BorderThicness),
                new PointF(drawingWidth - CustomAppearance.BorderThicness, drawingAngleSize),
                new PointF(drawingWidth - CustomAppearance.BorderThicness, drawingHeight - CustomAppearance.BorderAngle),
                new PointF(drawingWidth - CustomAppearance.BorderAngle - CustomAppearance.BorderThicness, drawingHeight),
                new PointF(drawingAngleSize, drawingHeight),
                new PointF(CustomAppearance.BorderThicness, drawingHeight - CustomAppearance.BorderAngle),
                new PointF(CustomAppearance.BorderThicness, drawingAngleSize),
                new PointF(drawingAngleSize, CustomAppearance.BorderThicness)
            };

            graphics.FillPolygon(new SolidBrush(backColor), points);
            graphics.DrawPolygon(new Pen(borderColor, CustomAppearance.BorderThicness), points);
            if (!string.IsNullOrEmpty(Text))
                graphics.DrawString(Text, Font, new SolidBrush(foreColor), new Point(leftOffset, topOffset));
            if (Image != null && !string.IsNullOrEmpty(Text))
            {
                graphics.DrawImage(Image, new Point(leftOffset - Image.Width, topOffset));
            }
            else if (BackgroundImage != null)
            {
                graphics.DrawImage(BackgroundImage, 0, 0, Width, Height);
            }
        }

        private enum ButtonEvents
        {
            NoEvent,
            MouseEnter,
            MouseLeave,
            MouseDown,
            MouseUp
        }
    }
}
