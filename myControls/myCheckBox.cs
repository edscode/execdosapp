﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Windows.Forms;


namespace ProGammaX
{
    public class CustomBaseUIAppearanceTypeConverter : ExpandableObjectConverter
    {
        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
        {
            // if destination type is the type we expected try to convert
            if (destinationType == typeof(string))
            {
                CustomBaseUIAppearance appearance = value as CustomBaseUIAppearance;
                // if the value is of type appearance as we need it
                if (appearance != null)
                {
                    return string.Format("Back { 0}; Border { 1}; Thickness[{ 2}]", appearance.BackColor.ToString(), appearance.BorderColor.ToString(), appearance.BorderThicness.ToString());
                }
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }
    }

    [TypeConverter(typeof(CustomBaseUIAppearanceTypeConverter))]
    public class CustomBaseUIAppearance
    {
        private Control _owner;
        private Color _borderColor;
        private Color _backColor;
        private float _borderThicness;
        private bool _enableAntiAlias;
        protected Control Owner
        {
            get
            {
                return _owner;
            }
        }
        [Description("Gets or Sets control’s border color.It can not be set to Color.Transparent."), Category("Custom Appearance"), DefaultValue(typeof(Color), "Black")]
        public Color BorderColor
        {
            get { return _borderColor; }
            set
            {
                if (value.Equals(Color.Transparent))
                {
                    throw new ArgumentOutOfRangeException("BorderColor", "Parameter cannot be set to Color.Transparent");
                }
                if (!_borderColor.Equals(value))
                {
                    _borderColor = value;
                    _owner.Invalidate();
                }
            }
        }
        [Description("Gets or Sets control’s background color.It can not be set to Color.Transparent."), Category("Custom Appearance"), DefaultValue(typeof(Color), "White")]
        public Color BackColor
        {
            get { return _backColor; }
            set
            {
                if (value.Equals(Color.Transparent))
                {
                    throw new ArgumentOutOfRangeException("BackColor", "Parameter cannot be set to Color.Transparent");
                }
                if (!_backColor.Equals(value))
                {
                    _backColor = value;
                    _owner.Invalidate();
                }
            }
        }
        [Description("Gets or Sets control’s border thickness."), Category("Custom Appearance"), DefaultValue(1)]
        public float BorderThicness
        {
            get { return _borderThicness; }
            set
            {
                if (value < 1)
                {
                    throw new ArgumentOutOfRangeException("BorderThicness", "Parameter must be set to a valid integer value hihger than 0.");
                }
                if (_borderThicness != value)
                {
                    _borderThicness = value;
                    _owner.Invalidate();
                }
            }
        }
        [Description("If true control’s is drawn using antialiasing, otherwise antialiasing is not enabled."), Category("Custom Appearance"), DefaultValue(true)]
        public bool EnableAntiAlias
        {
            get { return _enableAntiAlias; }
            set
            {
                if (_enableAntiAlias != value)
                {
                    _enableAntiAlias = value;
                    _owner.Invalidate();
                }
            }
        }
        public CustomBaseUIAppearance(Control owner)
        {
            if (owner == null)
            {
                throw new ArgumentNullException("owner");
            }
            _owner = owner;
            _borderColor = Color.Black;
            _backColor = Color.White;
            _borderThicness = 1;
            _enableAntiAlias = true;
        }
    }

    [TypeConverter(typeof(CustomBaseUIAppearanceTypeConverter))]
    public class CustomCheckBoxUIAppearance : CustomBaseUIAppearance
    {
        private Color _checkedBorderColor;
        private Color _checkedBackColor;
        private Color _tickColor;
        private int _tickThickness;

        [Description("Gets or Sets check box’s border color when checked.It can not be set to Color.Transparent."), Category("Custom Appearance"), DefaultValue(typeof(Color), "Black")]
        public Color CheckedBorderColor
        {
            get { return _checkedBorderColor; }
            set
            {
                if (value.Equals(Color.Transparent))
                {
                    throw new ArgumentOutOfRangeException("CheckedBorderColor", "Parameter cannot be set to Color.Transparent");
                }
                if (!_checkedBorderColor.Equals(value))
                {
                    _checkedBorderColor = value;
                    Owner.Invalidate();
                }
            }
        }

        [Description("Gets or Sets check box’s background color when checked.It can not be set to Color.Transparent."), Category("Custom Appearance"), DefaultValue(typeof(Color), "White")]
        public Color CheckedBackColor
        {
            get { return _checkedBackColor; }
            set
            {
                if (value.Equals(Color.Transparent))
                {
                    throw new ArgumentOutOfRangeException("CheckedBackColor", "Parameter cannot be set to Color.Transparent");
                }
                if (!_checkedBackColor.Equals(value))
                {
                    _checkedBackColor = value;
                    Owner.Invalidate();
                }
            }
        }

        [Description("Gets or Sets check box’s tick color.It can not be set to Color.Transparent."), Category("Custom Appearance"), DefaultValue(typeof(Color), "Black")]
        public Color TickColor
        {
            get { return _tickColor; }
            set
            {
                if (value.Equals(Color.Transparent))
                {
                    throw new ArgumentOutOfRangeException("TickColor", "Parameter cannot be set to Color.Transparent");
                }
                if (!_tickColor.Equals(value))
                {
                    _tickColor = value;
                    Owner.Invalidate();
                }
            }
        }

        [Description("Gets or Sets check box’s tick thickness."), Category("Custom Appearance"), DefaultValue(1)]
        public int TickThickness
        {
            get { return _tickThickness; }
            set
            {
                if (value < 1) { throw new ArgumentOutOfRangeException("TickThickness", "Parameter must be set to a valid integer value hihger than 0."); }
                if (_tickThickness != value) { _tickThickness = value; Owner.Invalidate(); }
            }
        }

        public CustomCheckBoxUIAppearance(Control owner) : base(owner)
        {
            _checkedBorderColor = Color.Black;
            _checkedBackColor = Color.White;
            _tickColor = Color.Black;
            _tickThickness = 2;
        }
    }

    public class myCheckBox : CheckBox
    {
        private bool _enableCustomUIAppearance;

        [Description("If true control’s is drawn using custom UI appearance, otherwise it appears using standard drawing."), Category("Custom Appearance"), DefaultValue(true), DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public bool EnableCustomUIAppearance
        {
            get { return _enableCustomUIAppearance; }
            set
            {
                if (_enableCustomUIAppearance != value)
                {
                    _enableCustomUIAppearance = value;
                    if (AutoSize)
                    {
                        AutoSize = false;
                    }
                    Invalidate();
                }
            }
        }
        [Description("This property specifies the way of drawing the control."), Category("Custom Appearance"),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public CustomCheckBoxUIAppearance CustomAppearance
        {
            get;
            set;
        }
        public myCheckBox()
        {
            _enableCustomUIAppearance = true;
        }
        protected override void OnPaint(PaintEventArgs pevent)
        {
            // Call base class’ OnPaint
            base.OnPaint(pevent);

            try
            {
                // MainForm.mainFormRef.IsApplicationIdle(this);

                if (!MainForm.mainFormRef.checkBox_OPTIONS_Themes_UseSkinOnControls.Checked)
                {
                    return;

                }
            }
            catch
            {
                return;
            }


            if (!_enableCustomUIAppearance)
            {
                return;
            }
            // padding of the standard CheckBox
            int offset = 2;
            // distance betwen CechBox area and included label
            int distance = 5;
            // tick’s part height placed above the border
            int tickOffset = 6;
            // both faces of check box square are 11 pixels
            int checkBoxWidth = 11;
            Graphics graphics = pevent.Graphics;
            graphics.Clear(BackColor);
            // get Text measure according to selected Font
            SizeF stringMeasure = graphics.MeasureString(Text, Font);
            // Set graphics object to paint nice using antialias.
            if (CustomAppearance.EnableAntiAlias)
            {
                graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            }
            // calculate offsets
            int leftOffset = offset + Padding.Left;
            int topOffset = (int)(ClientRectangle.Height - stringMeasure.Height) / 2;
            if (topOffset < 0) { topOffset = offset + Padding.Top; } else { topOffset += Padding.Top; }
            if (Checked)
            { // Fill CheckBox's rectangle 
                graphics.FillRectangle(new SolidBrush(CustomAppearance.CheckedBackColor), leftOffset, topOffset + tickOffset, checkBoxWidth, checkBoxWidth);
                // Draw Checkox's rectangle 
                if (Enabled)
                    graphics.DrawRectangle(new Pen(CustomAppearance.CheckedBorderColor, CustomAppearance.BorderThicness), leftOffset, topOffset + tickOffset, checkBoxWidth, checkBoxWidth);
                // Draw tick
                Point[] points = new Point[]
                {
                    new Point(leftOffset, topOffset + tickOffset + CustomAppearance.TickThickness),
                    new Point(leftOffset + (int)(checkBoxWidth / 2), topOffset + (checkBoxWidth - CustomAppearance.TickThickness) + tickOffset),
                    new Point(leftOffset + checkBoxWidth + 1, topOffset)
                };

                graphics.DrawLines(new Pen(CustomAppearance.TickColor, CustomAppearance.TickThickness), points);
            }
            else
            { // Fill CheckBox's rectangle
                graphics.FillRectangle(new SolidBrush(CustomAppearance.BackColor), leftOffset, topOffset + tickOffset, checkBoxWidth, checkBoxWidth);
                // Draw Checkox's rectangle 
                if (Enabled)
                    graphics.DrawRectangle(new Pen(CustomAppearance.BorderColor, CustomAppearance.BorderThicness), leftOffset, topOffset + tickOffset, checkBoxWidth, checkBoxWidth);
            }

            if (Enabled)
                graphics.DrawString(Text, Font, new SolidBrush(ForeColor), new Point(leftOffset + checkBoxWidth + distance, topOffset));
            else
                graphics.DrawString(Text, Font, new SolidBrush(ThemedColors.DisabledMenuTextColor), new Point(leftOffset + checkBoxWidth + distance, topOffset));
        }

        protected override void OnAutoSizeChanged(EventArgs e)
        {
            if (_enableCustomUIAppearance)
            {
                if (AutoSize)
                {
                    AutoSize = false;
                }
            }

            base.OnAutoSizeChanged(e);
        }
    }
}