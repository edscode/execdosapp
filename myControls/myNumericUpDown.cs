﻿#region Directives
using System;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Collections.Generic;
using System.ComponentModel;
#endregion

namespace ProGammaX
{
    class myNumericUpDown : NumericUpDown
    {
        private Bitmap Skin = new Bitmap(1, 1);
        private Dictionary<IntPtr, cNumericUpDown> _oScrollbarSkin;

        protected override void OnEnabledChanged(EventArgs e)
        {
            base.OnEnabledChanged(e);

            SetColors();
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);

            try
            {
                Start();
            }
            catch
            {

            }
        }

        private void SetColors()
        {
            this.BackColor = this.Enabled ? ThemedColors.WindowColor : ThemedColors.BackColor;
            this.ForeColor = this.Enabled ? ThemedColors.ForeColor : ThemedColors.DisabledMenuTextColor;
        }

        protected override void OnVisibleChanged(EventArgs e)
        {
            base.OnVisibleChanged(e);

            SetColors();

            try
            {
                if (this.Visible && this.FindForm().Visible)
                {
                    Start();
                }
            }
            catch
            {
            }
        }

        private void Start()
        {
            if (!MainForm.mainFormRef.checkBox_OPTIONS_Themes_UseSkinOnControls.Checked)
            {
                return;
            }

            AddScrollBar();
        }

        private void LoadImages()
        {
            Skin.Dispose();

            Skin = new Bitmap(MainForm.mainFormRef.vienna_spin);
        }

        /// <summary>
        /// Remove a specific control from the skin engine
        /// </summary>
        /// <param name="handle">Control handle</param>
        public void RemoveScrollBar(IntPtr handle)
        {
            if (handle != IntPtr.Zero)
            {
                Control ctl = Control.FromHandle(handle);

                if (ctl != null)
                {
                    _oScrollbarSkin[ctl.Handle].Dispose();
                    _oScrollbarSkin.Remove(ctl.Handle);
                }
            }
        }

        /// <summary>
        /// Add method for the ScrollBar control
        /// </summary>
        /// <param name="ct">Control type [ScrollBar]</param>
        /// <param name="track">Track image</param>
        /// <param name="arrow">Arrow image</param>
        /// <param name="thumb">Thumb image</param>
        /// <param name="orientation">ScrollbBar orientation</param>
        public void AddScrollBar()
        {
            if (_oScrollbarSkin == null)
                _oScrollbarSkin = new Dictionary<IntPtr, cNumericUpDown>();

            var control = Control.FromHandle(this.Handle);
            if (control is myNumericUpDown)
            {
                if (_oScrollbarSkin.ContainsKey(control.Handle))
                {
                    RemoveScrollBar(control.Handle);
                }

                if (control.Visible)
                {
                    LoadImages();
                    _oScrollbarSkin.Add(control.Handle, new cNumericUpDown(control.Handle, Skin));
                }

                control.Refresh();
            }
        }
    }
}
