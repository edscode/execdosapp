﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Reflection;
using System.Runtime.InteropServices;
using ProGammaX;

namespace Splitter
{



    public enum Direction
    {
        Right = 0,
        Left,
        Up,
        Down
    }

    public enum Docked
    {
        Right = 0,
        Left,
        Up,
        Down
    }
    public partial class mySplitter : SplitContainer
    {
        public mySplitter()
        {
            //this.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ResizeRedraw = true;
            this.BackColor = ProGammaX.ThemedColors.BackColor;
            this.ForeColor = ProGammaX.ThemedColors.ForeColor;
            mouseHover = false;

            this.DoubleBuffered = true;

            SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            //this.SetStyle(ControlStyles.AllPaintingInWmPaint |
            //                  ControlStyles.UserPaint |
            //                  ControlStyles.OptimizedDoubleBuffer, true);

            //MethodInfo objMethodInfo = typeof(Control).GetMethod("SetStyle", BindingFlags.NonPublic | BindingFlags.Instance);

            //object[] objArgs = new object[] { ControlStyles.AllPaintingInWmPaint |
            //                                                    ControlStyles.UserPaint |
            //                                                   ControlStyles.OptimizedDoubleBuffer, true };

            //objMethodInfo.Invoke(this.Panel1, objArgs);
            //objMethodInfo.Invoke(this.Panel2, objArgs);



            //SendMessage(this.Handle, LVM_SETHOTCURSOR, IntPtr.Zero, Cursors.Arrow.Handle);

            //this.ClientSizeChanged += new System.EventHandler(this.VisibleChanged1);
            //this.VisibleChanged += new System.EventHandler(this.VisibleChanged1);


        }

        //public const uint LVM_SETHOTCURSOR = 4158;

        //[DllImport("user32.dll")]
        //public static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, IntPtr wParam, IntPtr lParam);



        Bitmap backImage = (Bitmap)(ProGammaX.Properties.Resources.Arrow_Disabled);

        // definitions
        private Direction startDirection = Direction.Left;
        public Direction splitterDirection { get { return startDirection; } set { startDirection = value; } }

        private double SplitterDistanceRatio = 0.35;
        public double splitterDistanceRatio { get { return SplitterDistanceRatio; } set { SplitterDistanceRatio = value; } }


        //private Bitmap PreviousBackgroundImage = null;
        //public Bitmap previousBackgroundImage { get { return PreviousBackgroundImage; } set { PreviousBackgroundImage = value; } }

        private double splitterPanelMinSize1 = 0.25;
        public double splitterPanelMinSize { get { return splitterPanelMinSize1; } set { splitterPanelMinSize1 = value; } }

        private double splitterPanelMaxSize1 = 0.75;
        public double splitterPanelMaxSize { get { return splitterPanelMaxSize1; } set { splitterPanelMaxSize1 = value; } }


        public void setSplitterParrameters()
        {

            //this.SplitterWidth = splitterWidth;
            changeDirectionAfterChangingOrientation();
            //if (splitterIsDocked == false)
            //{

            //    this.Panel1MinSize = (int)(this.ClientSize.Width * splitterPanelMinSize);
            //    this.Panel2MinSize = (int)(this.ClientSize.Width - this.ClientSize.Width * splitterPanelMaxSize);
            //}
            //else
            //{
            evaluateIfIsDockedOrNot();
            //}
        }






        public bool splitterIsDocked = false;
        //public Direction splitterDirection;
        public Bitmap splitterArrowIsDockedAndDisabled(Docked direction)
        {

            Bitmap bmp = null;


            if (this.Orientation == System.Windows.Forms.Orientation.Horizontal)
            {
                if (direction == Docked.Up)
                {
                    // up arrow
                    bmp = (Bitmap)(ProGammaX.Properties.Resources.Arrow_Disabled);
                    bmp.RotateFlip(RotateFlipType.Rotate270FlipNone);

                }
                if (direction == Docked.Down)
                {
                    // down arrow
                    bmp = (Bitmap)(ProGammaX.Properties.Resources.Arrow_Disabled);
                    bmp.RotateFlip(RotateFlipType.Rotate90FlipNone);
                }
            }
            else
            {
                if (direction == Docked.Right)
                {
                    // right arrow
                    bmp = (Bitmap)(ProGammaX.Properties.Resources.Arrow_Disabled);

                }
                if (direction == Docked.Left)
                {
                    // left arrow
                    bmp = (Bitmap)(ProGammaX.Properties.Resources.Arrow_Disabled);
                    bmp.RotateFlip(RotateFlipType.Rotate180FlipNone);
                }
            }
            // decide which direction the arrow will point

            return bmp;


        }
        public Bitmap splitterArrowIsDockedAndEnabled(Docked direction)
        {

            Bitmap bmp = null;


            if (this.Orientation == System.Windows.Forms.Orientation.Horizontal)
            {
                if (direction == Docked.Up)
                {
                    // up arrow
                    bmp = (Bitmap)(ProGammaX.Properties.Resources.Arrow);
                    bmp.RotateFlip(RotateFlipType.Rotate270FlipNone);

                }
                if (direction == Docked.Down)
                {
                    // down arrow
                    bmp = (Bitmap)(ProGammaX.Properties.Resources.Arrow);
                    bmp.RotateFlip(RotateFlipType.Rotate90FlipNone);
                }
            }
            else
            {
                if (direction == Docked.Right)
                {
                    // right arrow
                    bmp = (Bitmap)(ProGammaX.Properties.Resources.Arrow);

                }
                if (direction == Docked.Left)
                {
                    // left arrow
                    bmp = (Bitmap)(ProGammaX.Properties.Resources.Arrow);
                    bmp.RotateFlip(RotateFlipType.Rotate180FlipNone);
                }
            }
            // decide which direction the arrow will point

            return bmp;


        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            sizeChanged();
        }


        private void sizeChanged()
        {
            //try
            //{
            //    if (Visible)
            //        MainForm.mainFormRef.IsApplicationIdle(this);
            //}
            //catch
            //{

            //}

            try
            {
                if (this.Visible)
                {

                    setSplitterParrameters();

                    if (this.Orientation == System.Windows.Forms.Orientation.Horizontal)
                    {

                        if (splitterIsDocked == false)
                        {
                            this.SplitterDistance = (int)((double)this.ClientSize.Height * splitterDistanceRatio);
                        }
                        backImage = getBitmapWithDirection_Disabled(splitterDirection);
                        Cursor.Current = System.Windows.Forms.Cursors.HSplit;
                        //splitterDistanceRatio = (double)this.SplitterDistance / (double)this.ClientSize.Height;
                    }
                    else
                    {

                        if (splitterIsDocked == false)
                        {
                            this.SplitterDistance = (int)((double)this.ClientSize.Width * splitterDistanceRatio);
                        }
                        backImage = getBitmapWithDirection_Disabled(splitterDirection);
                        Cursor.Current = System.Windows.Forms.Cursors.VSplit;
                        //splitterDistanceRatio = (double)this.SplitterDistance / (double)this.ClientSize.Width;

                    }
                    
                    //if (Visible)
                    //this.Refresh();
                }

            }
            catch
            {
            }


        }


        protected override void OnVisibleChanged(EventArgs e)
        {
            base.OnVisibleChanged(e);
            sizeChanged();
        }


        //private void VisibleChanged1(object sender, EventArgs e)
        //{

        //    sizeChanged();
        //}



        private Bitmap getBitmapWithDirection_Disabled(Direction direction)
        {



            Bitmap bmp = null;


            // decide which direction the arrow will point
            if (direction == Direction.Right)
            {
                // right arrow
                if (splitterIsDocked)
                {
                    bmp = splitterArrowIsDockedAndDisabled(Docked.Left);
                }
                else
                {
                    bmp = (Bitmap)(ProGammaX.Properties.Resources.Arrow_Disabled);
                }
            }
            if (direction == Direction.Left)
            {
                // left arrow
                if (splitterIsDocked)
                {
                    bmp = splitterArrowIsDockedAndDisabled(Docked.Right);
                }
                else
                {
                    bmp = (Bitmap)(ProGammaX.Properties.Resources.Arrow_Disabled);
                    bmp.RotateFlip(RotateFlipType.Rotate180FlipNone);
                }
            }
            if (direction == Direction.Up)
            {
                // up arrow
                if (splitterIsDocked)
                {
                    bmp = splitterArrowIsDockedAndDisabled(Docked.Down);
                }
                else
                {
                    bmp = (Bitmap)(ProGammaX.Properties.Resources.Arrow_Disabled);
                    bmp.RotateFlip(RotateFlipType.Rotate270FlipNone);
                }
            }
            if (direction == Direction.Down)
            {
                // down arrow
                if (splitterIsDocked)
                {
                    bmp = splitterArrowIsDockedAndDisabled(Docked.Up);
                }
                else
                {
                    bmp = (Bitmap)(ProGammaX.Properties.Resources.Arrow_Disabled);
                    bmp.RotateFlip(RotateFlipType.Rotate90FlipNone);
                }
            }

            return bmp;

        }
        private Bitmap getBitmapWithDirection_Enabled(Direction direction)
        {



            Bitmap bmp = null;


            // decide which direction the arrow will point
            if (direction == Direction.Right)
            {
                // right arrow
                if (splitterIsDocked)
                {
                    bmp = splitterArrowIsDockedAndEnabled(Docked.Left);
                }
                else
                {
                    bmp = (Bitmap)(ProGammaX.Properties.Resources.Arrow);
                }
            }
            if (direction == Direction.Left)
            {
                // left arrow
                if (splitterIsDocked)
                {
                    bmp = splitterArrowIsDockedAndEnabled(Docked.Right);
                }
                else
                {
                    bmp = (Bitmap)(ProGammaX.Properties.Resources.Arrow);
                    bmp.RotateFlip(RotateFlipType.Rotate180FlipNone);
                }
            }
            if (direction == Direction.Up)
            {
                // up arrow
                if (splitterIsDocked)
                {
                    bmp = splitterArrowIsDockedAndEnabled(Docked.Down);
                }
                else
                {
                    bmp = (Bitmap)(ProGammaX.Properties.Resources.Arrow);
                    bmp.RotateFlip(RotateFlipType.Rotate270FlipNone);
                }
            }
            if (direction == Direction.Down)
            {
                // down arrow
                if (splitterIsDocked)
                {
                    bmp = splitterArrowIsDockedAndEnabled(Docked.Up);
                }
                else
                {
                    bmp = (Bitmap)(ProGammaX.Properties.Resources.Arrow);
                    bmp.RotateFlip(RotateFlipType.Rotate90FlipNone);
                }
            }

            return bmp;

        }
        private void changeDirectionAfterChangingOrientation()
        {

            if (this.Orientation == System.Windows.Forms.Orientation.Horizontal)
            {
                if (splitterDirection == Direction.Left || splitterDirection == Direction.Right)
                {
                    splitterDirection = Direction.Down;
                }
            }
            else
            {
                if (splitterDirection == Direction.Down || splitterDirection == Direction.Up)
                {
                    splitterDirection = Direction.Left;
                }

            }
        }


        bool mouseIsDown = false;
        bool mouseHover = false;
        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);

            this.IsSplitterFixed = true;
            mouseIsDown = true;
        }
        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);
            mouseIsDown = false;
            mouseHover = true;
            this.IsSplitterFixed = false;
        }
        protected override void OnMouseHover(EventArgs e)
        {
            base.OnMouseHover(e);
            //mouseIsDown = false;
            mouseHover = true;


        }
        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);
            mouseHover = false;


            backImage = getBitmapWithDirection_Disabled(splitterDirection);




            if (Visible)
                this.Refresh();

        }




        private void chooseBitmapImage()
        {



            if (this.Orientation == System.Windows.Forms.Orientation.Horizontal)
            {


                if (mousePositionX >= (SplitterRectangle.X + SplitterRectangle.Width / 2 - arrowRectangleWidth / 2) && mousePositionX <= (SplitterRectangle.X + SplitterRectangle.Width / 2 + arrowRectangleWidth / 2))
                {

                    Cursor.Current = System.Windows.Forms.Cursors.Hand;


                    backImage = getBitmapWithDirection_Enabled(splitterDirection);
                    //backImage = getBitmapWithDirection_Disabled(splitterDirection);




                }
                else
                {

                    Cursor.Current = System.Windows.Forms.Cursors.HSplit;

                    backImage = getBitmapWithDirection_Disabled(splitterDirection);




                }




            }
            else
            {


                if (mousePositionY >= (SplitterRectangle.Y + SplitterRectangle.Height / 2 - arrowRectangleHeight / 2) && mousePositionY <= (SplitterRectangle.Y + SplitterRectangle.Height / 2 + arrowRectangleHeight / 2))
                {

                    Cursor.Current = System.Windows.Forms.Cursors.Hand;

                    backImage = getBitmapWithDirection_Enabled(splitterDirection);
                    //backImage = getBitmapWithDirection_Disabled(splitterDirection);





                }
                else
                {

                    Cursor.Current = System.Windows.Forms.Cursors.VSplit;


                    backImage = getBitmapWithDirection_Disabled(splitterDirection);




                }



            }



        }



        protected override void OnPaintBackground(PaintEventArgs paintEvent)
        {
            base.OnPaintBackground(paintEvent);

            //if (Visible)
            //    this.Update();
        }




        private int mousePositionX = 0;
        private int mousePositionY = 0;

        protected override void OnMouseMove(MouseEventArgs e)
        {


            // Check to make sure the splitter won't be updated by the
            // normal move behavior also
            if (this.IsSplitterFixed)
            {
                // Make sure that the button used to move the splitter
                // is the left mouse button
                if (e.Button.Equals(MouseButtons.Left))
                {
                    // Checks to see if the splitter is aligned Vertically
                    if (this.Orientation.Equals(Orientation.Vertical))
                    {
                        // Only move the splitter if the mouse is within
                        // the appropriate bounds
                        if (e.X > 0 && e.X < (this.Width))
                        {
                            // Move the splitter & force a visual refresh
                            this.SplitterDistance = e.X;
                            //this.Refresh();
                        }
                    }
                    // If it isn't aligned vertically then it must be
                    // horizontal
                    else
                    {
                        // Only move the splitter if the mouse is within
                        // the appropriate bounds
                        if (e.Y > 0 && e.Y < (this.Height))
                        {
                            // Move the splitter & force a visual refresh
                            this.SplitterDistance = e.Y;
                            //this.Refresh();
                        }
                    }
                }
                // If a button other than left is pressed or no button
                // at all
                else
                {
                    // This allows the splitter to be moved normally again
                    this.IsSplitterFixed = false;
                }
            }


            changeDirectionAfterChangingOrientation();

            mousePositionX = e.X;
            mousePositionY = e.Y;



            chooseBitmapImage();







            if (mouseIsDown && splitterIsDocked == false)
            {
                if (this.Orientation == System.Windows.Forms.Orientation.Horizontal)
                {



                    if (this.SplitterDistance >= (int)(this.ClientSize.Height * splitterPanelMinSize))
                    {
                        if (mousePositionY >= 0)
                        {

                            try
                            {
                                this.SplitterDistance = mousePositionY;
                                splitterDistanceRatio = (double)this.SplitterDistance / (double)this.ClientSize.Height;
                            }
                            catch
                            {
                            }

                        }

                    }
                    else
                    {
                        this.SplitterDistance = (int)(this.ClientSize.Height * splitterPanelMinSize);
                    }


                }
                else
                {

                    if (this.SplitterDistance >= (int)(this.ClientSize.Width * splitterPanelMinSize))
                    {
                        if (mousePositionX >= 0)
                        {

                            try
                            {

                                this.SplitterDistance = mousePositionX;
                                splitterDistanceRatio = (double)this.SplitterDistance / (double)this.ClientSize.Width;

                            }
                            catch
                            {
                            }


                        }

                    }
                    else
                    {
                        this.SplitterDistance = (int)(this.ClientSize.Width * splitterPanelMinSize);
                    }

                }



            }

            mouseHover = true;

            if (Visible && !mouseIsDown)
                this.Refresh();


        }

        protected override void OnMouseClick(MouseEventArgs e)
        {

            //this.ResizeRedraw = true;
            if (this.Orientation == System.Windows.Forms.Orientation.Horizontal)
            {
                if (e.X >= (SplitterRectangle.X + SplitterRectangle.Width / 2 - arrowRectangleWidth / 2) && e.X <= (SplitterRectangle.X + SplitterRectangle.Width / 2 + arrowRectangleWidth / 2))
                {
                    if (this.SplitterDistance >= (int)(this.ClientSize.Height * splitterPanelMinSize) && this.SplitterDistance <= (int)(this.ClientSize.Height * splitterPanelMaxSize))
                    {



                        splitterIsDocked = true;




                    }
                    else
                    {


                        splitterIsDocked = false;


                    }
                }


                //splitterDistanceRatio = (double)this.SplitterDistance / (double)this.ClientSize.Height;



            }
            else
            {
                if (e.Y >= (SplitterRectangle.Y + SplitterRectangle.Height / 2 - arrowRectangleHeight / 2) && e.Y <= (SplitterRectangle.Y + SplitterRectangle.Height / 2 + arrowRectangleHeight / 2))
                {
                    if (this.SplitterDistance >= (int)(this.ClientSize.Width * splitterPanelMinSize) && this.SplitterDistance <= (int)(this.ClientSize.Width * splitterPanelMaxSize))
                    {
                        //this.Panel1.Left = (int)(this.ClientSize.Height * 0.25);

                        splitterIsDocked = true;





                    }
                    else
                    {

                        //this.Panel1.Left = 0;

                        splitterIsDocked = false;

                    }
                }

                //splitterDistanceRatio = (double)this.SplitterDistance / (double)this.ClientSize.Width;

            }


            //evaluate results
            //evaluateIfIsDockedOrNot();



            if (Visible)
                this.Refresh();


        }


        private void evaluateIfIsDockedOrNot()
        {

            Direction direction = splitterDirection;
            //backImage.RotateFlip(RotateFlipType.Rotate270FlipNone);
            try
            {
                // decide which direction the arrow will point
                if (direction == Direction.Right)
                {
                    // right arrow
                    if (splitterIsDocked)
                    {
                        this.Panel1MinSize = (int)(this.ClientSize.Width * splitterPanelMinSize);
                        this.Panel2MinSize = 0;
                        this.SplitterDistance = this.ClientSize.Width;
                    }
                    else
                    {
                        if (this.SplitterDistance > (int)(this.ClientSize.Width * splitterPanelMaxSize))
                        {
                            this.SplitterDistance = (int)((double)this.ClientSize.Width * splitterDistanceRatio);

                        }
                        this.Panel1MinSize = (int)(this.ClientSize.Width * splitterPanelMinSize);
                        this.Panel2MinSize = (int)(this.ClientSize.Width * splitterPanelMinSize);
                    }

                }
                if (direction == Direction.Left)
                {
                    // left arrow
                    if (splitterIsDocked)
                    {
                        this.Panel1MinSize = 0;
                        this.Panel2MinSize = (int)(this.ClientSize.Width * splitterPanelMinSize);
                        this.SplitterDistance = 0;
                    }
                    else
                    {
                        if (this.SplitterDistance < (int)(this.ClientSize.Width * splitterPanelMinSize))
                        {
                            this.SplitterDistance = (int)((double)this.ClientSize.Width * splitterDistanceRatio);

                        }
                        this.Panel1MinSize = (int)(this.ClientSize.Width * splitterPanelMinSize);
                        this.Panel2MinSize = (int)(this.ClientSize.Width * splitterPanelMinSize);

                    }

                }
                if (direction == Direction.Up)
                {
                    // up arrow
                    if (splitterIsDocked)
                    {
                        this.Panel1MinSize = 0;
                        this.Panel2MinSize = (int)(this.ClientSize.Height * splitterPanelMinSize);
                        this.SplitterDistance = 0;
                    }
                    else
                    {
                        if (this.SplitterDistance < (int)(this.ClientSize.Width * splitterPanelMinSize))
                        {
                            this.SplitterDistance = (int)((double)this.ClientSize.Height * splitterDistanceRatio);

                        }
                        this.Panel1MinSize = (int)(this.ClientSize.Height * splitterPanelMinSize);
                        this.Panel2MinSize = (int)(this.ClientSize.Height * splitterPanelMinSize);

                    }

                }
                if (direction == Direction.Down)
                {
                    // down arrow
                    if (splitterIsDocked)
                    {
                        this.Panel1MinSize = (int)(this.ClientSize.Height * splitterPanelMinSize);
                        this.Panel2MinSize = 0;
                        this.SplitterDistance = this.ClientSize.Height;
                    }
                    else
                    {
                        if (this.SplitterDistance > (int)(this.ClientSize.Height * splitterPanelMaxSize))
                        {
                            this.SplitterDistance = (int)((double)this.ClientSize.Height * splitterDistanceRatio);

                        }
                        this.Panel1MinSize = (int)(this.ClientSize.Height * splitterPanelMinSize);
                        this.Panel2MinSize = (int)(this.ClientSize.Height * splitterPanelMinSize);
                    }

                }
            }
            catch
            {
            }

            //backImage = getBitmapWithDirection_Disabled(direction);
        }


        //protected override void OnGotFocus(EventArgs e)
        //{
        //    this.Refresh();

        //}


        int arrowRectangleHeight = 0;
        int arrowRectangleWidth = 0;


        protected override void OnPaint(PaintEventArgs pe)
        {
            // Calling the base class OnPaint

            base.OnPaint(pe);
            Graphics g = pe.Graphics;

            evaluateIfIsDockedOrNot();

            using (var brush = mouseHover ? new SolidBrush(ProGammaX.ThemedColors.SelectionColor) : new SolidBrush(ProGammaX.ThemedColors.BackColor))
            {
                Rectangle r = this.SplitterRectangle;
                if (mouseHover)
                {
                    //mouseHover = false;
                    g.FillRectangle(brush, r);
                }
                else
                {
                    mouseHover = false;
                    g.FillRectangle(brush, r);
                }
            }

            if (this.Orientation == System.Windows.Forms.Orientation.Horizontal)
            {
                //this.SplitterWidth = 24;
                arrowRectangleWidth = backImage.Width; //(int)(SplitterRectangle.Width / 8);

                Rectangle split_rect = new Rectangle(SplitterRectangle.X + SplitterRectangle.Width / 2 - arrowRectangleWidth / 2, SplitterRectangle.Y, arrowRectangleWidth, SplitterRectangle.Height);


                g.DrawImage(backImage, split_rect);

            }
            else
            {
                //this.SplitterWidth = 24;
                arrowRectangleHeight = backImage.Height; //(int)(SplitterRectangle.Height / 8);

                Rectangle split_rect = new Rectangle(SplitterRectangle.X, SplitterRectangle.Y + SplitterRectangle.Height / 2 - arrowRectangleHeight / 2, SplitterRectangle.Width, arrowRectangleHeight);

                g.DrawImage(backImage, split_rect);
                //g.DrawImage(ProGammaX.Properties.Resources.Arrow, split_rect);

            }




            //previousBackgroundImage = backImage;

           // this.Update();







        }






    }




}