﻿using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System;
using System.Runtime.InteropServices;

namespace ProGammaX
{
    /// <summary>
    /// Description of CustomTabControl. http://www.codeproject.com/script/Articles/ViewDownloads.aspx?aid=13305
    /// </summary>
    [ToolboxBitmap(typeof(TabControl))]
    public class myTabControl : TabControl
    {



        public enum TabStyle
        {
            Normal = 0,
            Level1,
            Level2,
            Level3
        }

        // definitions
        private TabStyle tabStyle = TabStyle.Normal;
        public TabStyle TabStyleMode
        {
            get { return tabStyle; }
            set { tabStyle = value; }
        }




        public myTabControl()
            : base()
        {
            if (this._DisplayManager.Equals(TabControlDisplayManager.Custom))
            {
                this.SetStyle(ControlStyles.UserPaint, true);
                this.ItemSize = new Size(0, 15);
                this.Padding = new Point(9, 0);
            }
            this.DrawMode = TabDrawMode.OwnerDrawFixed;

            this.Appearance = TabAppearance.Normal;

            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            this.ResizeRedraw = true;
        }

        TabControlDisplayManager _DisplayManager = TabControlDisplayManager.Custom;

        [System.ComponentModel.DefaultValue(typeof(TabControlDisplayManager), "Custom")]
        public TabControlDisplayManager DisplayManager
        {
            get
            {
                return this._DisplayManager;
            }
            set
            {
                if (this._DisplayManager != value)
                {
                    if (this._DisplayManager.Equals(TabControlDisplayManager.Custom))
                    {
                        this.SetStyle(ControlStyles.UserPaint, true);
                        this.ItemSize = new Size(0, 15);
                        this.Padding = new Point(9, 0);
                    }
                    else
                    {
                        this.ItemSize = new Size(0, 0);
                        this.Padding = new Point(6, 3);
                        this.SetStyle(ControlStyles.UserPaint, false);
                    }
                }
            }
        }

        protected override void OnPaintBackground(PaintEventArgs pevent)
        {
            base.OnPaintBackground(pevent);

            if (this.DesignMode == true)
            {
                LinearGradientBrush backBrush = new LinearGradientBrush(
                              this.Bounds,
                              ThemedColors.BackColor,
                              ThemedColors.SelectionColor,
                              LinearGradientMode.Vertical);
                pevent.Graphics.FillRectangle(backBrush, this.Bounds);
                backBrush.Dispose();
            }
            else
            {
                this.PaintTransparentBackground(pevent.Graphics, this.ClientRectangle);
            }
        }

        protected void PaintTransparentBackground(Graphics g, Rectangle clipRect)
        {
            if ((this.Parent != null))
            {
                clipRect.Offset(this.Location);
                PaintEventArgs e = new PaintEventArgs(g, clipRect);
                GraphicsState state = g.Save();
                g.SmoothingMode = SmoothingMode.HighSpeed;
                try
                {
                    g.TranslateTransform((float)-this.Location.X, (float)-this.Location.Y);
                    this.InvokePaintBackground(this.Parent, e);
                    this.InvokePaint(this.Parent, e);
                }

                finally
                {
                    g.Restore(state);
                    clipRect.Offset(-this.Location.X, -this.Location.Y);
                }
            }
            else
            {
                System.Drawing.Drawing2D.LinearGradientBrush backBrush = new System.Drawing.Drawing2D.LinearGradientBrush(this.Bounds, ThemedColors.BackColor, ThemedColors.BackColor, System.Drawing.Drawing2D.LinearGradientMode.Vertical);
                g.FillRectangle(backBrush, this.Bounds);
                backBrush.Dispose();
            }
        }

        //protected override void OnSelecting(TabControlCancelEventArgs e)
        //{
        //    base.OnSelecting(e);
        //    try
        //    {
        //        MainForm.mainFormRef.IsApplicationIdle(this);

        //        if (MainForm.mainFormRef.prolongedOperationIsRunning)
        //            e.Cancel = true;
        //    }
        //    catch
        //    {
        //    }

        //}

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            try
            {
                if (!this.Visible)
                {
                    return;
                }
            }
            catch
            {
                return;
            }
            //   Paint the Background
            this.PaintTransparentBackground(e.Graphics, this.ClientRectangle);

            this.PaintAllTheTabs(ref e);
            this.PaintTheTabPageBorder(ref e);
            this.PaintTheSelectedTab(ref e);
        }

        private void PaintAllTheTabs(ref PaintEventArgs e)
        {
            if (this.TabCount > 0)
            {
                for (int index = 0; index < this.TabCount; index++)
                {
                    this.PaintTab(ref e, index);
                }
            }
        }

        private void PaintTab(ref PaintEventArgs e, int index)
        {
            base.OnPaint(e);
            GraphicsPath path = this.GetPath(index);
            this.PaintTabBackground(e.Graphics, index, path);
            this.PaintTabBorder(e.Graphics, index, path);
            this.PaintTabText(e.Graphics, index);
            this.PaintTabImage(e.Graphics, index);
        }

        private void PaintTabBackground(Graphics graph, int index, GraphicsPath path)
        {



            Rectangle rect = this.GetTabRect(index);
            PathGradientBrush pthGrBrush = new PathGradientBrush(path);

            //System.Drawing.Brush buttonBrush =
            //    new System.Drawing.Drawing2D.LinearGradientBrush(
            //        rect,
            //        ThemedColors.WindowColor,
            //        ThemedColors.BackColor,
            //        LinearGradientMode.Vertical);
            //        //SystemColors.ControlLightLight,
            //        //SystemColors.ControlLight,
            //        //LinearGradientMode.Vertical);



            // Use the path to construct a brush.



            if (index == getHoverTabIndex && getHoverTabIndex >= 0 && index != this.SelectedIndex)
            {


                pthGrBrush.CenterColor = ThemedColors.MenuColor;
                pthGrBrush.SurroundColors = new Color[] { ThemedColors.TabPageBorderColor };
                this.Invalidate();
                //getHoverTabIndex = -1;

            }
            else
            {


                // Set the color at the center of the path to blue.
                // Set the color along the entire boundary  
                // of the path to aqua.
                pthGrBrush.CenterColor = ThemedColors.MenuColor;
                pthGrBrush.SurroundColors = new Color[] { ThemedColors.BackColor };

            }


            if (index == this.SelectedIndex)
            {
                //buttonBrush = buttonBrush =
                //new System.Drawing.Drawing2D.LinearGradientBrush(
                //    rect,
                //    ThemedColors.BackColor,
                //    ThemedColors.SelectionColor,
                //    LinearGradientMode.Vertical); //new System.Drawing.SolidBrush(ThemedColors.SelectionColor);
                ////ControlPaint.DrawButton(graph, rect, ButtonState.Checked);



                // Use the path to construct a brush.
                pthGrBrush = new PathGradientBrush(path);
                // Set the color at the center of the path to blue.
                pthGrBrush.CenterColor = ThemedColors.MenuColor;
                pthGrBrush.SurroundColors = new Color[] { ThemedColors.TabPageBorderColor };



            }



            //Pen borderPen = new Pen(ThemedColors.SelectionColor, 2);
            graph.FillPath(pthGrBrush, path);
            //graph.DrawPath(borderPen, path);

            pthGrBrush.Dispose();
        }

        private void PaintTabBorder(Graphics graph, int index, System.Drawing.Drawing2D.GraphicsPath path)
        {

            Pen borderPen = new Pen(ThemedColors.TabPageBorderColor, 2);
            //if (getHoverTabIndex >= 0)
            //{
            //    if (index == getHoverTabIndex)
            //    {
            //        borderPen = new Pen(ThemedColors.MenuColor, 2);
            //    }
            //}
            graph.DrawPath(borderPen, path);
            borderPen.Dispose();
        }

        private void PaintTabImage(Graphics graph, int index)
        {
            Image tabImage = null;
            if (this.TabPages[index].ImageIndex > -1 && this.ImageList != null)
            {
                tabImage = this.ImageList.Images[this.TabPages[index].ImageIndex];
            }
            else if (this.TabPages[index].ImageKey.Trim().Length > 0 && this.ImageList != null)
            {
                tabImage = this.ImageList.Images[this.TabPages[index].ImageKey];
            }
            if (tabImage != null)
            {
                Rectangle rect = this.GetTabRect(index);
                graph.DrawImage(tabImage, rect.Right - rect.Height - 8, 4, rect.Height - 4, rect.Height - 4);
            }
        }

        private void PaintTabText(System.Drawing.Graphics graph, int index)
        {
            Rectangle rect = this.GetTabRect(index);
            Rectangle rect2 = new Rectangle(rect.Left + 2, rect.Top + 1, rect.Width - 6, rect.Height);

            //if (index == 0 && this.TabStyleMode == TabStyle.Level3) rect2 = new Rectangle(rect.Left + rect.Height, rect.Top + 1, rect.Width - rect.Height, rect.Height);




            string tabtext = this.TabPages[index].Text;

            System.Drawing.StringFormat format = new System.Drawing.StringFormat();
            format.Alignment = StringAlignment.Near;
            format.LineAlignment = StringAlignment.Center;
            format.Trimming = StringTrimming.EllipsisCharacter;
            format.FormatFlags = StringFormatFlags.NoWrap;

            SolidBrush forebrush = null;

            if (this.TabPages[index].Enabled == false)
            {
                //forebrush = SystemBrushes.ControlDark;
                forebrush = new SolidBrush(ThemedColors.ForeColor);
            }
            else
            {
                forebrush = new SolidBrush(ThemedColors.MenuTextColor); //SystemBrushes.ControlText;
            }

            Font tabFont = new Font(this.Font.FontFamily, rect.Height / 4, FontStyle.Italic);
            if (index == this.SelectedIndex)
            {
                tabFont = new Font(this.Font.FontFamily, rect.Height / 4, FontStyle.Bold);
                //forebrush = new SolidBrush(ThemedColors.SelectionForeColor);

                //if (index == 0)
                //{
                //    rect2 = new Rectangle(rect.Left + rect.Height, rect.Top + 1, rect.Width - rect.Height + 5, rect.Height);
                //}
            }

            graph.DrawString(tabtext, tabFont, forebrush, rect2, format);

        }

        private void PaintTheTabPageBorder(ref PaintEventArgs e)
        {
            base.OnPaint(e);

            if (this.TabCount > 0)
            {
                Rectangle borderRect = this.TabPages[0].Bounds;
                borderRect.Inflate(1, 1);
                ControlPaint.DrawBorder(e.Graphics, borderRect, ThemedColors.TabPageBorderColor, ButtonBorderStyle.Solid);
            }
        }

        private void PaintTheSelectedTab(ref PaintEventArgs e)
        {
            base.OnPaint(e);

            Rectangle selrect;
            int selrectRight = 0;

            switch (this.SelectedIndex)
            {
                case -1:
                    break;
                case 0:
                    selrect = this.GetTabRect(this.SelectedIndex);
                    selrectRight = selrect.Right;
                    e.Graphics.DrawLine(new Pen(ThemedColors.BackColor), selrect.Left + 2, selrect.Bottom + 1, selrectRight - 2, selrect.Bottom + 1);
                    break;
                default:
                    selrect = this.GetTabRect(this.SelectedIndex);
                    selrectRight = selrect.Right;
                    e.Graphics.DrawLine(new Pen(ThemedColors.BackColor), selrect.Left + 6 - selrect.Height, selrect.Bottom + 1, selrectRight - 2, selrect.Bottom + 1);
                    break;
            }
        }

        private System.Drawing.Drawing2D.GraphicsPath GetPath(int index)
        {
            System.Drawing.Drawing2D.GraphicsPath path = new System.Drawing.Drawing2D.GraphicsPath();
            path.Reset();


            float angle = 0;
            Rectangle rect = this.GetTabRect(index);
            var rotationPoint = new PointF(rect.Width / 2, rect.Height / 2);



            //if (index == 0)
            //{
            //    path.AddLine(rect.Left + 1, rect.Bottom + 1, rect.Left + rect.Height, rect.Top + 2);
            //    path.AddLine(rect.Left + rect.Height + 4, rect.Top, rect.Right - 3, rect.Top);
            //    path.AddLine(rect.Right - 1, rect.Top + 2, rect.Right - 1, rect.Bottom + 1);
            //}
            //else
            //{

            byte margin = 2;
            byte bevel = 8;
            PointF[] points;

            switch (TabStyleMode)
            {
                case TabStyle.Level1:
                    {


                        bevel = 16;
                        points = new PointF[]
                                  {
                                    new PointF(rect.Left + bevel + margin, rect.Bottom - margin),
                                    new PointF(rect.Left + bevel / 4 + margin, rect.Bottom - bevel / 4 - margin),
                                    new PointF(rect.Left + margin, rect.Bottom - bevel - margin)
                                  };
                        path.AddCurve(points, 0.8f);

                        points = new PointF[]
                                {
                                    //new PointF(rect.Left + rect.Width / 2 + margin, rect.Bottom),
                                    new PointF(rect.Left + margin, rect.Bottom - bevel - margin),
                                    new PointF(rect.Left + margin, rect.Top + bevel + margin)
                                    //new PointF(rect.Left + bevel + margin, rect.Top + margin),
                                    //new PointF(rect.Right - bevel - margin, rect.Top + margin),
                                    //new PointF(rect.Right - margin, rect.Top + bevel + margin),
                                    //new PointF(rect.Right - margin, rect.Bottom - bevel - margin),
                                    //new PointF(rect.Right - rect.Width / 2 - margin, rect.Bottom)                                    
                                  };
                        path.AddLines(points);

                        points = new PointF[]
                                  {
                                    new PointF(rect.Left + margin, rect.Top + bevel + margin),
                                    new PointF(rect.Left + bevel / 4 + margin, rect.Top + bevel / 4 + margin),
                                    new PointF(rect.Left + bevel + margin, rect.Top + margin)
                                  };
                        path.AddCurve(points, 0.8f);

                        points = new PointF[]
                                  {
                                    new PointF(rect.Left + bevel + margin, rect.Top + margin),
                                    new PointF(rect.Right - bevel - margin, rect.Top + margin)
                                  };
                        path.AddLines(points);

                        points = new PointF[]
                                {
                                    new PointF(rect.Right - bevel - margin, rect.Top + margin),
                                    new PointF(rect.Right - bevel / 4 - margin, rect.Top + bevel / 4 + margin),
                                    new PointF(rect.Right - margin, rect.Top + bevel + margin),
                                  };
                        path.AddCurve(points, 0.8f);

                        points = new PointF[]
                                  {
                                    new PointF(rect.Right - margin, rect.Top + bevel + margin),
                                    new PointF(rect.Right - margin, rect.Bottom - bevel - margin)
                                  };
                        path.AddLines(points);

                        points = new PointF[]
                                {
                                    new PointF(rect.Right - margin, rect.Bottom - bevel - margin),
                                    new PointF(rect.Right - bevel / 4 - margin, rect.Bottom - bevel / 4 - margin),
                                    new PointF(rect.Right - bevel - margin, rect.Bottom - margin),
                                  };
                        path.AddCurve(points, 0.8f);

                        points = new PointF[]
                                  {
                                    new PointF(rect.Right - bevel - margin, rect.Bottom - margin),
                                    new PointF(rect.Left + bevel + margin, rect.Bottom - margin)
                                  };
                        path.AddLines(points);









                        break;
                    }
                case TabStyle.Level2:
                    {


                        points = new PointF[]{
                        new PointF(rect.Left + margin, rect.Top + bevel + margin),
                        new PointF(rect.Left + bevel + margin, rect.Top + margin),
                        new PointF(rect.Right - bevel - margin, rect.Top + margin),
                        new PointF(rect.Right - margin, rect.Top + bevel + margin),
                        new PointF(rect.Right - margin, rect.Bottom - bevel - margin),
                        new PointF(rect.Right - bevel - margin, rect.Bottom - margin),
                        new PointF(rect.Left + bevel + margin, rect.Bottom - margin),
                        new PointF(rect.Left + margin, rect.Bottom - bevel - margin),
                        new PointF(rect.Left + margin, rect.Top + bevel + margin)
                        };
                        //path.CloseFigure();
                        path.AddPolygon(points);


                        break;
                    }
                case TabStyle.Level3:
                    {






                        points = new PointF[]{
                        new PointF(rect.Left + margin, rect.Bottom),
                        new PointF(rect.Left + margin, rect.Top + bevel + margin),
                        new PointF(rect.Left + bevel + margin, rect.Top + margin),
                        new PointF(rect.Right - bevel - margin, rect.Top + margin),
                        new PointF(rect.Right - margin, rect.Top + bevel + margin),
                        new PointF(rect.Right - margin, rect.Bottom),
                        new PointF(rect.Left + margin, rect.Bottom)
                        //new PointF(rect.Left + margin, rect.Top + 8 + margin),
                        //new PointF(rect.Left + 8 + margin, rect.Top + margin),
                        //new PointF(rect.Right - 8 - margin, rect.Top + margin),
                        //new PointF(rect.Right - margin, rect.Top + 8 + margin),
                        //new PointF(rect.Right - margin, rect.Bottom),
                        //new PointF(rect.Left + margin, rect.Bottom),
                                  };
                        //path.CloseFigure();
                        path.AddPolygon(points);





                        //if (index == 0)
                        //{
                        //    path.AddLine(rect.Left + 1, rect.Bottom + 1, rect.Left + rect.Height, rect.Top + 2);
                        //    path.AddLine(rect.Left + rect.Height + 4, rect.Top, rect.Right - 3, rect.Top);
                        //    path.AddLine(rect.Right - 1, rect.Top + 2, rect.Right - 1, rect.Bottom + 1);
                        //}
                        //else
                        //{
                        //    if (index == this.SelectedIndex)
                        //    {
                        //        path.AddLine(rect.Left + 1, rect.Top + 5, rect.Left + 4, rect.Top + 2);
                        //        path.AddLine(rect.Left + 8, rect.Top, rect.Right - 3, rect.Top);
                        //        path.AddLine(rect.Right - 1, rect.Top + 2, rect.Right - 1, rect.Bottom + 1);
                        //        path.AddLine(rect.Right - 1, rect.Bottom + 1, rect.Left + 1, rect.Bottom + 1);
                        //    }
                        //    else
                        //    {
                        //    path.AddLine(rect.Left, rect.Top + 6, rect.Left + 4, rect.Top + 2);
                        //    path.AddLine(rect.Left + 8, rect.Top, rect.Right - 3, rect.Top);
                        //    path.AddLine(rect.Right - 1, rect.Top + 2, rect.Right - 1, rect.Bottom + 1);
                        //    path.AddLine(rect.Right - 1, rect.Bottom + 1, rect.Left, rect.Bottom + 1);
                        //}
                        //}



                        //points = new PointF[]
                        //{
                        //new PointF(rect.Left + margin, rect.Top + 8 + margin),
                        //new PointF(rect.Left + 8 + margin, rect.Top + margin),
                        //new PointF(rect.Right - 8 - margin, rect.Top + margin),
                        //new PointF(rect.Right - margin, rect.Top + 8 + margin),
                        //new PointF(rect.Right - margin, rect.Bottom - 16 - margin),
                        //new PointF(rect.Right - 8 - margin, rect.Bottom - 8 - margin),
                        //new PointF(rect.Right - rect.Width / 2 - margin, rect.Bottom),
                        //new PointF(rect.Left + 8 + margin, rect.Bottom - 8 - margin),
                        //new PointF(rect.Left + margin, rect.Bottom - 16 - margin),
                        //new PointF(rect.Left + margin, rect.Top + 8 + margin)
                        //};



                        break;
                    }
                default:
                    {



                        if (index == this.SelectedIndex)
                        {
                            //path.AddLine(rect.Left + 1, rect.Top + 5, rect.Left + 4, rect.Top + 2);
                            //path.AddLine(rect.Left + 8, rect.Top, rect.Right - 3, rect.Top);
                            //path.AddLine(rect.Right - 1, rect.Top + 2, rect.Right - 1, rect.Bottom + 1);
                            //path.AddLine(rect.Right - 1, rect.Bottom + 1, rect.Left + 1, rect.Bottom + 1);
                            path.AddLine(rect.Left + margin, rect.Top + rect.Height / 2 + margin, rect.Left + margin, rect.Bottom);
                            path.AddArc(new RectangleF(rect.Left + margin, rect.Top + margin, rect.Height / 2, rect.Height / 2), 180f, 90f);
                            path.AddLine(rect.Left + rect.Height / 2 + margin, rect.Top + margin, rect.Right - rect.Height / 2 - margin, rect.Top + margin);
                            path.AddArc(new RectangleF(rect.Right - rect.Height / 2 - margin, rect.Top + margin, rect.Height / 2, rect.Height / 2), 270f, 90f);
                            path.AddLine(rect.Right - margin, rect.Top + rect.Height / 2 + margin, rect.Right - margin, rect.Bottom);
                            path.AddLine(rect.Right, rect.Bottom + margin, rect.Left, rect.Bottom + margin);

                        }
                        else
                        {
                            //path.AddLine(rect.Left, rect.Top + 6, rect.Left + 4, rect.Top + 2);
                            //path.AddLine(rect.Left + 8, rect.Top, rect.Right - 3, rect.Top);
                            //path.AddLine(rect.Right - 1, rect.Top + 2, rect.Right - 1, rect.Bottom + 1);
                            //path.AddLine(rect.Right - 1, rect.Bottom + 1, rect.Left, rect.Bottom + 1);


                            path.AddLine(rect.Left + margin, rect.Top + rect.Height / 2 + margin, rect.Left + margin, rect.Bottom);
                            path.AddArc(new RectangleF(rect.Left + margin, rect.Top + margin, rect.Height / 2, rect.Height / 2), 180f, 90f);
                            path.AddLine(rect.Left + rect.Height / 2 + margin, rect.Top + margin, rect.Right - rect.Height / 2 - margin, rect.Top + margin);
                            path.AddArc(new RectangleF(rect.Right - rect.Height / 2 - margin, rect.Top + margin, rect.Height / 2, rect.Height / 2), 270f, 90f);
                            path.AddLine(rect.Right - margin, rect.Top + rect.Height / 2 + margin, rect.Right - margin, rect.Bottom);
                            path.AddLine(rect.Right, rect.Bottom + 1, rect.Left, rect.Bottom + 1);





                        }





                        break;
                    }



            }




            //path.CloseFigure();

            var m = new Matrix();


            //rotate background
            switch (this.Alignment)
            {
                case TabAlignment.Bottom:
                    {
                        angle = 180;
                        break;
                    }
                case TabAlignment.Left:
                    {
                        angle = 270;
                        break;
                    }
                case TabAlignment.Right:
                    {
                        angle = 90;
                        break;
                    }
                default:
                    {
                        angle = 0;
                        break;
                    }
            }
            //var m = new Matrix(1, 0, 0, -1, 0, rect.Height);
            //m.Reset();


            m.Translate(0, 0);
            m.RotateAt(angle, new PointF(rect.X + rect.Width / 2, rect.Y + rect.Height / 2));
            path.Transform(m);



            //}
            return path;
        }

        public enum TabControlDisplayManager
        {
            Default,
            Custom
        }

        [DllImport("user32.dll")]
        private static extern IntPtr SendMessage(IntPtr hWnd, int Msg, IntPtr wParam, IntPtr lParam);

        private const int WM_SETFONT = 0x30;
        private const int WM_FONTCHANGE = 0x1d;

        protected override void OnCreateControl()
        {
            base.OnCreateControl();
            this.OnFontChanged(EventArgs.Empty);
        }

        protected override void OnFontChanged(EventArgs e)
        {
            base.OnFontChanged(e);
            IntPtr hFont = this.Font.ToHfont();
            SendMessage(this.Handle, WM_SETFONT, hFont, (IntPtr)(-1));
            SendMessage(this.Handle, WM_FONTCHANGE, IntPtr.Zero, IntPtr.Zero);
            this.UpdateStyles();
            int tempWidth = 0;
            try
            {
                tempWidth += this.ImageList.ImageSize.Width;

            }
            catch
            {
            }
            this.ItemSize = new Size(tempWidth, (int)this.Font.SizeInPoints + 30);
        }



        private int getHoverTabIndex = -1;
        protected override void OnMouseMove(MouseEventArgs e)
        {

            Rectangle mouseRect = new Rectangle(e.X, e.Y, 1, 1);
            getHoverTabIndex = -1;
            Cursor.Current = Cursors.Default;

            for (int i = 0; i < this.TabCount; i++)
            {


                if (this.GetTabRect(i).IntersectsWith(mouseRect))
                {
                    getHoverTabIndex = i;

                    Cursor.Current = Cursors.Hand;


                    break;
                }


            }
            this.Invalidate();
            base.OnMouseMove(e);


        }

        protected override void OnLeave(EventArgs e)
        {
            base.OnLeave(e);
            getHoverTabIndex = -1;
            Cursor.Current = Cursors.Default;
            this.Invalidate();
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnLeave(e);
            getHoverTabIndex = -1;
            Cursor.Current = Cursors.Default;
            this.Invalidate();
        }

        protected override void OnSelectedIndexChanged(EventArgs e)
        {
            base.OnSelectedIndexChanged(e);
            //this.Refresh();
        }





















    }
}
